﻿namespace ImageProcessPlugins
{
    partial class ScissorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.view = new Common.View();
            this.SuspendLayout();
            // 
            // view
            // 
            this.view.BackColor = System.Drawing.SystemColors.Window;
            this.view.Dock = System.Windows.Forms.DockStyle.Fill;
            this.view.EnableMoving = true;
            this.view.Location = new System.Drawing.Point(0, 0);
            this.view.Name = "view";
            this.view.Size = new System.Drawing.Size(703, 499);
            this.view.TabIndex = 0;
            this.view.ViewMode = Common.ViewMode.CustomZoomFactor;
            this.view.ZoomFactor = 20;
            this.view.Paint += new System.Windows.Forms.PaintEventHandler(this.view_Paint);
            this.view.ImageMouseMove += new Common.View.ImageMouseEvent(this.view_ImageMouseMove);
            this.view.ImageMouseDown += new Common.View.ImageMouseEvent(this.view_ImageMouseDown);
            this.view.ImageMouseUp += new Common.View.ImageMouseEvent(this.view_ImageMouseUp);
            this.view.DrawOverlay += new Common.View.DrawOverlayDelegate(this.view_DrawOverlay);
            // 
            // ScissorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(703, 499);
            this.Controls.Add(this.view);
            this.Name = "ScissorForm";
            this.Text = "ScissorForm";
            this.ResumeLayout(false);

        }

        #endregion

        private Common.View view;
    }
}