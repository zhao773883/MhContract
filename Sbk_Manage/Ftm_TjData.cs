﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ZedGraph;

namespace Sbk_Manage
{
    public partial class Ftm_TjData : Form
    {
        public Ftm_TjData()
        {
            InitializeComponent();
        }
        private void Ftm_TjData_Load(object sender, EventArgs e)
        {
            TjNsbl();
        }


        //年设备量
        public void TjNsbl()
        {
            ZedGraphControl zgc = this.zedGraphControl1;
            string sqlstr = "select strftime('%Y', ysrq) as ysrq1,count(*) as sbsl from  sb_info  group by ysrq1 order by ysrq1";
            DataTable dt = Program.SqliteDB.GetDataTable(sqlstr);
            List<string> xlst = new List<string>();
            List<double> ylst = new List<double>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                xlst.Add(dt.Rows[i]["ysrq1"].ToString());
                ylst.Add(Convert.ToDouble(dt.Rows[i]["sbsl"].ToString()));
            }
            zgc.SuspendLayout();
            zgc.Controls.Clear();
            zgc.GraphPane.CurveList.Clear();
            zgc.GraphPane.GraphObjList.Clear();  
            //不可缩放
            zgc.ZoomButtons = MouseButtons.None;
            zgc.ZoomButtons2 = MouseButtons.None;
            zgc.ZoomStepFraction = 0;
            //
            GraphPane myPane = zgc.GraphPane;
            myPane.Title.Text = "年设备量";
            myPane.XAxis.Title.Text = "年份";
            myPane.YAxis.Title.Text = "设备数";

            string[] xLables = xlst.ToArray();
            double[] yvalue = ylst.ToArray();

            // 设置x轴为文本显示
            myPane.XAxis.Type = AxisType.Text;
            // 设置x轴文本标签值
            myPane.XAxis.Scale.TextLabels = xLables;
            //myPane.XAxis.Scale.FontSpec.Angle = 45f;
            // 为每个“柱子”上方添加值标签
            for (int i = 0; i < yvalue.Length; i++)
            {
                double Y = yvalue[i];
                TextObj text = new TextObj(Y.ToString(), (i + 1), Y + 3.0);
                text.FontSpec.Border.IsVisible = false;
                text.FontSpec.Fill.IsVisible = false;
                myPane.GraphObjList.Add(text);
            }
            BarItem myCurve = myPane.AddBar("",null,yvalue, Color.Orange);  
            myPane.BarSettings.Type = BarType.SortedOverlay;
            // 设置背景色
            myPane.Chart.Fill = new Fill(Color.White, Color.LightGoldenrodYellow, 45.0F);
            zgc.ResumeLayout();
            zgc.AxisChange();
            zgc.Refresh();
        }

        //年合同数
        public void TjNhts()
        {
            ZedGraphControl zgc = this.zedGraphControl2;
            string sqlstr = "select strftime('%Y', htqdrq) as htqdrq1,count(*) as htsl from  ht_info  group by htqdrq1 order by htqdrq1";
            DataTable dt = Program.SqliteDB.GetDataTable(sqlstr);
            List<string> xlst = new List<string>();
            List<double> ylst = new List<double>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                xlst.Add(dt.Rows[i]["htqdrq1"].ToString());
                ylst.Add(Convert.ToDouble(dt.Rows[i]["htsl"].ToString()));
            }
            zgc.SuspendLayout();
            zgc.Controls.Clear();
            zgc.GraphPane.CurveList.Clear();
            zgc.GraphPane.GraphObjList.Clear();
            //不可缩放
            zgc.ZoomButtons = MouseButtons.None;
            zgc.ZoomButtons2 = MouseButtons.None;
            zgc.ZoomStepFraction = 0;
            //
            GraphPane myPane = zgc.GraphPane;
            myPane.Title.Text = "年合同数";
            myPane.XAxis.Title.Text = "年份";
            myPane.YAxis.Title.Text = "合同数";

            string[] xLables = xlst.ToArray();
            double[] yvalue = ylst.ToArray();

            // 设置x轴为文本显示
            myPane.XAxis.Type = AxisType.Text;
            // 设置x轴文本标签值
            myPane.XAxis.Scale.TextLabels = xLables;
            //myPane.XAxis.Scale.FontSpec.Angle = 45f;
            // 为每个“柱子”上方添加值标签
            for (int i = 0; i < yvalue.Length; i++)
            {
                double Y = yvalue[i];
                TextObj text = new TextObj(Y.ToString(), (i + 1), Y + 3.0);
                text.FontSpec.Border.IsVisible = false;
                text.FontSpec.Fill.IsVisible = false;
                myPane.GraphObjList.Add(text);
            }
            BarItem myCurve = myPane.AddBar("", null, yvalue, Color.LightSkyBlue);
            myPane.BarSettings.Type = BarType.SortedOverlay;
            // 设置背景色
            myPane.Chart.Fill = new Fill(Color.White, Color.LightGoldenrodYellow, 45.0F);
            zgc.ResumeLayout();
            zgc.AxisChange();
            zgc.Refresh();
        }
        //年合同金额
        public void TjNhte()
        {
            ZedGraphControl zgc = this.zedGraphControl3;
            string sqlstr = "select strftime('%Y', htqdrq) as htqdrq1,sum(hte) as hte from  ht_info  group by htqdrq1 order by htqdrq1";
            DataTable dt = Program.SqliteDB.GetDataTable(sqlstr);
            List<string> xlst = new List<string>();
            List<double> ylst = new List<double>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                xlst.Add(dt.Rows[i]["htqdrq1"].ToString());
                ylst.Add(Convert.ToDouble(dt.Rows[i]["hte"].ToString()));
            }
            zgc.SuspendLayout();
            zgc.Controls.Clear();
            zgc.GraphPane.CurveList.Clear();
            zgc.GraphPane.GraphObjList.Clear();
            //不可缩放
            zgc.ZoomButtons = MouseButtons.None;
            zgc.ZoomButtons2 = MouseButtons.None;
            zgc.ZoomStepFraction = 0;
            //
            GraphPane myPane = zgc.GraphPane;
            myPane.Title.Text = "年合同额(万¥)";
            myPane.XAxis.Title.Text = "年份";
            myPane.YAxis.Title.Text = "合同额(万¥)";

            string[] xLables = xlst.ToArray();
            double[] yvalue = ylst.ToArray();

            // 设置x轴为文本显示
            myPane.XAxis.Type = AxisType.Text;
            // 设置x轴文本标签值
            myPane.XAxis.Scale.TextLabels = xLables;
            //myPane.XAxis.Scale.FontSpec.Angle = 45f;
            // 为每个“柱子”上方添加值标签
            for (int i = 0; i < yvalue.Length; i++)
            {
                double Y = yvalue[i];
                TextObj text = new TextObj(Y.ToString(), (i + 1), Y);
                text.FontSpec.Angle = 35f;//控制 文字 倾斜度
                text.FontSpec.Border.IsVisible = false;
                text.FontSpec.Fill.IsVisible = false;
                myPane.GraphObjList.Add(text);
            }
            BarItem myCurve = myPane.AddBar("", null, yvalue, Color.Pink);
            myPane.BarSettings.Type = BarType.SortedOverlay;
            // 设置背景色
            myPane.Chart.Fill = new Fill(Color.White, Color.LightGoldenrodYellow, 45.0F);
            zgc.ResumeLayout();
            zgc.AxisChange();
            zgc.Refresh();
        }
        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex == 0)
            {
                //年设备量
                TjNsbl();
            }
            else if (tabControl1.SelectedIndex == 1)
            {
                //年合同数
                TjNhts();
            } 
            else if (tabControl1.SelectedIndex == 2)
            {
                //年合同额
                TjNhte();
            }
        }
    }
}
